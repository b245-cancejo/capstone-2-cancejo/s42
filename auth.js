const jwt = require("jsonwebtoken");
//const user = require('./Models/userSchema')
const privateKey = "E-CommerseAPI";

module.exports.createToken = (user) => {

	//payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, privateKey, {});
}

//user verification
module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	if (typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, privateKey, (error, data) => {
			if (error){
				return response.status(403, "Error!, Please Register or login first!")
			}
			else{
				next();
			}
		})

	}
	else {
		return response.status(403, "Error!, Please Register or login first!")
	}
	
}

//admin verification
module.exports.adminVerify = (request, response, next) => {

	let token = request.headers.authorization;

	if (typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, privateKey, (error, data) => {
			if (error){
				return response.send({auth: "Failed."})
			}
			else{
				next();
			}
		})

	}
	else {
		return response.send({auth: "Failed."})
	}
	
}


module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		
		token = token.slice(7, token.length);
		return jwt.verify(token, privateKey, (err, data) => {
			if (err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}