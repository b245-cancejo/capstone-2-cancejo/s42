const user = require('../Models/userSchema')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const product = require('../Models/productSchema')

module.exports.register = (request, response) => {
    let input = request.body;

    user.findOne({email: input.email})
    .then(result => {
        if(result !== null){
            response.send(false)
        }else{
            let newUser = new user({
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                password: bcrypt.hashSync(input.password, 10)
            })
            newUser.save()
            .then(save => {
                return response.send(newUser)
            })
            .catch(error => response.send(error))
        }
    })
}

module.exports.login = async (request, response) => {
    const input = request.body;
    
    await user.findOne({email: input.email})
    .then(result => {
        if(result === null){
            return response.send(false)
        }else{
            const isPWCorrect = bcrypt.compareSync(input.password, result.password)

            if(isPWCorrect){
                
                return response.send({auth: auth.createToken(result)})

            }else{
                return response.send(false)
            }
        }
    })
    .catch(error => response.send(error))
}

module.exports.userDetails = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization)
    user.findById(userData.id)
    .then(result => {
        //console.log(result)
        return response.send(result)
    })
}

module.exports.addToCart = async (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const id = request.params.id;
    const input = request.body;

        await product.findById(id)
        .then(result => {
            if(result !== null){
                user.findById(userData.id)
                .then(result => {
                    result.cart = input;
                    result.save()
                    return response.send(result)
                })
            }
            else{
                response.send(false)
            }
            //result.cart = input;
            //result.save()
            //console.log(result)
            
        })
        
        .catch(error => response.send(error))
    
        
    

}