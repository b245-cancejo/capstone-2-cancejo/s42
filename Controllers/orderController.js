const user = require("../Models/userSchema.js");
const product = require("../Models/productSchema.js");
const order = require("../Models/orderSchema.js");
const auth = require("../auth");


module.exports.addOrder = async (request, response) => {

	const input = request.body;
	const idToBuy = request.params.id
	const userData = auth.decode(request.headers.authorization)

	await user.findById(userData.id)
			.then(result => {
				if(result.isAdmin){
					response.send(false)
					
				}
				else if(result !== null){
					
					//check the product if Exist
					product.findById(idToBuy)
						.then(result => {
							if (result !== null  && result.isActive){
								let newOrder = new order({
										user: userData.id,
										name: result.name,
                                        price: result.price,	
										quantity: input.quantity,
                                        totalPrice: result.price * input.quantity

									})
							
									newOrder.save()
									response.send(newOrder)

									// user.findById(userData._id)
									// .then(result => {
									// 	if (result !== null){
									// 		result.orders.push(newOrder.products[0])
									// 		//result.totalAmount.push(newOrder.totalAmount)
											
									// 		return result.save()
									// 	}else{
									// 		return false
									// 	}
									// })
																

							}else{
								response.send(false)
												
							}
						})
						
				}
				

			})					
	
}

module.exports.viewOrder = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(userData.id){
		await order.find({user: userData.id})
		.then(result => response.send(result))
	}
}