const product = require('../Models/productSchema')
const auth = require('../auth')

module.exports.allProducts = (request, response) => {
    const products = product.find({isActive: true})
    .then(result => {
        if (result === null){
            return response.send(false)
        }else{
            return response.send(result)
        }
    })
}

module.exports.getProductDetail = async (request, response) => {
    const id = request.params.id;
    await product.findById(id)
    .then(result => {
        if(result === null){
            return response.send(false);
        }
        else{
            return response.send(result)
        }
    })
}

module.exports.addProduct = (request, response) => {
    const input = request.body;
    const userData = auth.decode(request.headers.authorization);

    //console.log(userData.isAdmin)
    if(userData.isAdmin){

        let newProduct = new product({
            name: input.name,
            description: input.description,
            price: input.price,
            stocks: input.stocks
        });
        return newProduct.save()
        .then(products => response.send(products))
        .catch(error => response.send(error))
    }
    else{
        return response.send(false)
    }
    
}

module.exports.adminAllProducts = async (request, response) => {
    const userData = auth.decode(request.headers.authorization)
    if(userData.isAdmin){
       await product.find({})
        .then(result => {
            if (result === null){
                return response.send(false)
            }else{
                return response.send(result)
            }
        })

    }
   
}

module.exports.archiveProducts = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    const id = request.params.id;

    if(userData.isAdmin){
        product.findById(id)
        .then(result => {
            result.isActive = !result.isActive;
            result.save()
            return response.send(result)
        })
        
        .catch(error => response.send(error))
    }

}

module.exports.updateProducts = (request, response) => {
	let input = request.body
	let idToBeUpdate = request.params.id;
	const userData = auth.decode(request.headers.authorization);
	
	if(userData.isAdmin){
		product.findByIdAndUpdate(idToBeUpdate, input, {new: true})
		.then(result => {
			return response.send(result)
		})
		.catch(error => {
			return response.send(error)
		})
	}
	else{
		return response.send(false)
	}
	
}


