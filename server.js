const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const user = require('./Routes/userRoute');
const product = require('./Routes/productRoute');
const order = require('./Routes/orderRoute')
const port = 3010;

const app = express();

    //DATABASE CONNECTION
    mongoose.set('strictQuery', true);

    mongoose.connect('mongodb+srv://admin:admin@batch245-cancejo.kjh2l39.mongodb.net/Batch-245_E-Commerce_API_LazShop?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    let db = mongoose.connection;

    db.on("error", console.error.bind(console, "Connection Error!"))
    db.once("open", () => console.log("Database successfully connected!"))

    //MIDDLEWARES
    app.use(express.json());
    app.use(cors())
    app.use(express.urlencoded({extended: true}))

    //ROUTING
    app.use('/api/v1', user)
    app.use('/api/v1', product)
    app.use('/api/v1', order)

    app.listen(port, () => console.log(`Server is running at port ${port}`))

