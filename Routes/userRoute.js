const express = require('express');
const router = express.Router();
const auth = require("../auth")
const {register, login, userDetails, addToCart} = require('../Controllers/userController')

router.post('/register', register)

router.post('/login', login)

router.get('/userDetails',auth.verify, userDetails)

router.post('/addToCart/:id',auth.verify, addToCart)



module.exports = router;