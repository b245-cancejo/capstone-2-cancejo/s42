const express = require('express');
const router = express.Router();
const auth = require("../auth")
const {allProducts, getProductDetail, addProduct, adminAllProducts, archiveProducts, updateProducts} = require('../Controllers/productController')

router.get('/allProducts', allProducts);

router.get('/productDetail/:id', getProductDetail);

router.post('/admin/addProduct', auth.verify, addProduct);

router.get('/admin/allProduct', auth.verify, adminAllProducts);

router.put('/admin/archiveProducts/:id', auth.verify, archiveProducts);

router.put('/admin/updateProduct/:id', auth.verify, updateProducts);




module.exports = router;