
const express = require('express');
const router = express.Router();
const auth = require("../auth")
const {addOrder, viewOrder} = require('../Controllers/orderController')

router.post('/orderProduct/:id', auth.verify, addOrder);

router.get('/viewOrders', auth.verify, viewOrder);

module.exports = router;