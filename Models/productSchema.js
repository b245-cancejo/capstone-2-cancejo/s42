const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required!"]
	},
	description: {
		type: String,
		required: [true, "Product description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required!"]
	},
	ratings:{
		type: Number,
		default: 0.0
	},
	stocks: {
		type: Number,
		required: [true, 'Please enter product stock'],
		default: 0
	},
	numOfReviews: {
		type: Number,
		defaultdefault: 0
	},
	reviews: [
			{
				user: {
					type: mongoose.Schema.ObjectId,
					ref: 'User',
					required: true
				},
				name: {
					type: String,
					required:true
				},
				rating: {
					type: Number,
					required: true
				},
				comment: {
					type: String,
					required: true
				}
			}
		],
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date
	}
})

module.exports = mongoose.model("product", productSchema);